package com.bm.main.fpl.models;

public class X implements java.io.Serializable {
    private static final long serialVersionUID = -3175162002464574460L;
    private String response_code;
    private String notelp_pemilik;
    private String alamat_pemilik;
    private String keagenan;
    private String affiliasi;
    private XList_produk[] list_produk;
    private String nama_upline;
    private String link_afiliasi;
    private String cek_sum_logo;
    private String pinjaman_dana;
    private String rating;
    private String id_upline;
    private String expired_time;
    private String no_whatsapp_loket;
    private String perbankan;
    private String nama_logo;
    private String alias;
    private String bayar;
    private String fastravel;
    private String nama_pemilik;
    private String nama_loket;
    private String upgrade;
    private String response_desc;
    private String session_id;
    private String saldo;
    private String alamat_loket;
    private String notlp_upline;
    private String web_report;
    private String lion_parcel;
    private String tiketing;
    private String email_pemilik;
    private String belanja;
    private String id_outlet;
    private String edukasi;

    public String getResponse_code() {
        return this.response_code;
    }

    public void setResponse_code(String response_code) {
        this.response_code = response_code;
    }

    public String getNotelp_pemilik() {
        return this.notelp_pemilik;
    }

    public void setNotelp_pemilik(String notelp_pemilik) {
        this.notelp_pemilik = notelp_pemilik;
    }

    public String getAlamat_pemilik() {
        return this.alamat_pemilik;
    }

    public void setAlamat_pemilik(String alamat_pemilik) {
        this.alamat_pemilik = alamat_pemilik;
    }

    public String getKeagenan() {
        return this.keagenan;
    }

    public void setKeagenan(String keagenan) {
        this.keagenan = keagenan;
    }

    public String getAffiliasi() {
        return this.affiliasi;
    }

    public void setAffiliasi(String affiliasi) {
        this.affiliasi = affiliasi;
    }

    public XList_produk[] getList_produk() {
        return this.list_produk;
    }

    public void setList_produk(XList_produk[] list_produk) {
        this.list_produk = list_produk;
    }

    public String getNama_upline() {
        return this.nama_upline;
    }

    public void setNama_upline(String nama_upline) {
        this.nama_upline = nama_upline;
    }

    public String getLink_afiliasi() {
        return this.link_afiliasi;
    }

    public void setLink_afiliasi(String link_afiliasi) {
        this.link_afiliasi = link_afiliasi;
    }

    public String getCek_sum_logo() {
        return this.cek_sum_logo;
    }

    public void setCek_sum_logo(String cek_sum_logo) {
        this.cek_sum_logo = cek_sum_logo;
    }

    public String getPinjaman_dana() {
        return this.pinjaman_dana;
    }

    public void setPinjaman_dana(String pinjaman_dana) {
        this.pinjaman_dana = pinjaman_dana;
    }

    public String getRating() {
        return this.rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getId_upline() {
        return this.id_upline;
    }

    public void setId_upline(String id_upline) {
        this.id_upline = id_upline;
    }

    public String getExpired_time() {
        return this.expired_time;
    }

    public void setExpired_time(String expired_time) {
        this.expired_time = expired_time;
    }

    public String getNo_whatsapp_loket() {
        return this.no_whatsapp_loket;
    }

    public void setNo_whatsapp_loket(String no_whatsapp_loket) {
        this.no_whatsapp_loket = no_whatsapp_loket;
    }

    public String getPerbankan() {
        return this.perbankan;
    }

    public void setPerbankan(String perbankan) {
        this.perbankan = perbankan;
    }

    public String getNama_logo() {
        return this.nama_logo;
    }

    public void setNama_logo(String nama_logo) {
        this.nama_logo = nama_logo;
    }

    public String getAlias() {
        return this.alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getBayar() {
        return this.bayar;
    }

    public void setBayar(String bayar) {
        this.bayar = bayar;
    }

    public String getFastravel() {
        return this.fastravel;
    }

    public void setFastravel(String fastravel) {
        this.fastravel = fastravel;
    }

    public String getNama_pemilik() {
        return this.nama_pemilik;
    }

    public void setNama_pemilik(String nama_pemilik) {
        this.nama_pemilik = nama_pemilik;
    }

    public String getNama_loket() {
        return this.nama_loket;
    }

    public void setNama_loket(String nama_loket) {
        this.nama_loket = nama_loket;
    }

    public String getUpgrade() {
        return this.upgrade;
    }

    public void setUpgrade(String upgrade) {
        this.upgrade = upgrade;
    }

    public String getResponse_desc() {
        return this.response_desc;
    }

    public void setResponse_desc(String response_desc) {
        this.response_desc = response_desc;
    }

    public String getSession_id() {
        return this.session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getSaldo() {
        return this.saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getAlamat_loket() {
        return this.alamat_loket;
    }

    public void setAlamat_loket(String alamat_loket) {
        this.alamat_loket = alamat_loket;
    }

    public String getNotlp_upline() {
        return this.notlp_upline;
    }

    public void setNotlp_upline(String notlp_upline) {
        this.notlp_upline = notlp_upline;
    }

    public String getWeb_report() {
        return this.web_report;
    }

    public void setWeb_report(String web_report) {
        this.web_report = web_report;
    }

    public String getLion_parcel() {
        return this.lion_parcel;
    }

    public void setLion_parcel(String lion_parcel) {
        this.lion_parcel = lion_parcel;
    }

    public String getTiketing() {
        return this.tiketing;
    }

    public void setTiketing(String tiketing) {
        this.tiketing = tiketing;
    }

    public String getEmail_pemilik() {
        return this.email_pemilik;
    }

    public void setEmail_pemilik(String email_pemilik) {
        this.email_pemilik = email_pemilik;
    }

    public String getBelanja() {
        return this.belanja;
    }

    public void setBelanja(String belanja) {
        this.belanja = belanja;
    }

    public String getId_outlet() {
        return this.id_outlet;
    }

    public void setId_outlet(String id_outlet) {
        this.id_outlet = id_outlet;
    }

    public String getEdukasi() {
        return this.edukasi;
    }

    public void setEdukasi(String edukasi) {
        this.edukasi = edukasi;
    }
}
