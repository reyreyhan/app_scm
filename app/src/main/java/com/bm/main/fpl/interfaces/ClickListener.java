package com.bm.main.fpl.interfaces;

import android.view.View;

/**
 * Created by sarifhidayat on 2019-09-05.
 **/
public interface ClickListener {
    public void onClick(View view, int position);
    public void onLongClick(View view,int position);
}
