package com.bm.main.fpl.templates.clearableedittext;

public interface OnTextClearedListener {
    void onTextCleared();
}
