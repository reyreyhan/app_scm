package com.bm.main.fpl.models;

public class DownlineChildModel {

    private String notelp_pemilik2;
    private String nama_pemilik2;
    private String jumlah_downline2;
    private String nomor_whatsapp_outlet2;
    private String id_outlet2;

    public String getNotelp_pemilik2() {
        return this.notelp_pemilik2;
    }

    public void setNotelp_pemilik2(String notelp_pemilik2) {
        this.notelp_pemilik2 = notelp_pemilik2;
    }

    public String getNama_pemilik2() {
        return this.nama_pemilik2;
    }

    public void setNama_pemilik2(String nama_pemilik2) {
        this.nama_pemilik2 = nama_pemilik2;
    }

    public String getJumlah_downline2() {
        return this.jumlah_downline2;
    }

    public void setJumlah_downline2(String jumlah_downline2) {
        this.jumlah_downline2 = jumlah_downline2;
    }

    public String getNomor_whatsapp_outlet2() {
        return this.nomor_whatsapp_outlet2;
    }

    public void setNomor_whatsapp_outlet2(String nomor_whatsapp_outlet2) {
        this.nomor_whatsapp_outlet2 = nomor_whatsapp_outlet2;
    }

    public String getId_outlet2() {
        return this.id_outlet2;
    }

    public void setId_outlet2(String id_outlet2) {
        this.id_outlet2 = id_outlet2;
    }
}
