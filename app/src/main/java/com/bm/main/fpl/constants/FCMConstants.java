package com.bm.main.fpl.constants;

/**
 * Created by sarifhidayat on 7/24/17.
 */

public class FCMConstants {
    public static final String PUSH_TITLE = "title";
    public static final String PUSH_MESSAGE = "message";
    public static final String PUSH_URL = "image";
    public static boolean isActivityRunning = false;
    public static boolean isActivityDeposit = false;
    public static boolean isStillRunningRequest = false;

    public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
}
