package com.bm.main.fpl.constants;

/**
 * Created by sarifhidayat on 12/6/17.
 */

public class ProdukGroup {

    public static final String PLN = "PLN";
    public static final String PULSA = "PULSA";
    public static final String PDAM = "PDAM";
    public static final String PGN = "PGN";
    public static final String BPJS = "BPJS";
    public static final String INDIHOME = "INDIHOME";
    public static final String TELKOM = "TELKOM";
    public static final String FINANCE = "FINANCE";
    public static final String GAMEONLINE = "GAME ONLINE";
    public static final String EMONEY = "EMONEY";
    public static final String HPPASCA = "HP PASCA";
    public static final String KARTUKREDIT = "KARTU KREDIT";
    public static final String TVKABEL = "TV KABEL";
    public static final String ASURANSI = "ASURANSI";
    public static final String PERBANKAN = "PERBANKAN";
    public static final String PESAWAT = "PESAWAT";
    public static final String KERETA = "KERETA";
    public static final String PELNI = "PELNI";
    public static final String PESANANSAYA = "PESANANSAYA";
    public static final String PERTAGAS = "PERTAGAS";
    public static final String PAJAK = "PAJAK";
    public static final String PKB = "PKB";
}
