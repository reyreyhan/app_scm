package com.bm.main.fpl.constants;

/**
 * Created by sarifhidayat on 10/8/18.
 **/
public class RequestCode {

    public static final int ActionCode_ACCESS_FINE_LOCATION = 101;
    public static final int ActionCode_CAMERA = 102;
    public static final int ActionCode_READ_CONTACTS = 103;

//    public static final int ActionCode_READ_PHONE_STATE = 107;
//    public static final int ActionCode_READ_SMS = 108;
//    public static final int ActionCode_CALL_PHONE = 109;
//    public static final int ActionCode_READ_PHONE_NUMBER = 110;
//    public static final int ActionCode_SEND_SMS = 111;
//    public static final int ActionCode_WRITE_SMS = 112;
//    public static final int ActionCode_RECEIVED_SMS = 113;
    public static final int ActionCode_WRITE_EXTENAL_STORAGE = 114;
    public static final int ActionCode_READ_EXTERNAL_STORAGE = 115;
    public static final int ActionCode_GROUP_STORAGE = 116;
//    public static final int ActionCode_GROUP_SMS = 117;


//    public static final int ActionCode_GET_ACCOUNT = 104;
    public static final int ActionCode_TANGGAL_AWAL = 105;
    public static final int ActionCode_TANGGAL_AKHIR = 106;
    public static final int ActionCode_READ_SMS_OTP =107 ;
    public static final int ActionCode_SETTING =108 ;
//    public static final int ActionCode_READ_SMS_KEY = 108;

    public  static final int REQUEST_CHECK_SETTINGS_GPS=109;
    public static final int ActionCode_VOICE_RECOGNITION=110;
}
