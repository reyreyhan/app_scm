package com.bm.main.fpl.constants;

/**
 * Created by sarifhidayat on 11/30/18.
 **/
public class MenuActionCode {
    public static final int JUAL_BARANG = 1;
    public static final int PLN = 2;
    public static final int PULSA = 3;
    public static final int PDAM = 4;
    public static final int TIKET_TRAVEL = 5;
    public static final int BPJS = 6;
    public static final int INDIHOME = 7;
    public static final int TELKOM = 8;
    public static final int PASCABAYAR = 9;
    public static final int TVBERLANGGAN = 10;
    public static final int CICILAN = 11;
    public static final int GAMEONLINE = 12;
    public static final int EMONEY = 13;
    public static final int ASURANSI = 14;
    public static final int KARTUKREDIT = 15;
    public static final int PERBANKAN = 16;
    public static final int PGN = 17;
    public static final int EXPEDISI = 18;
    public static final int PINJAMDANA = 19;
    public static final int KEAGENAN = 20;
    public static final int BAYARBARANG = 21;
    public static final int BNI = 22;
    public static final int MANDIRI = 23;
    public static final int DOKU = 24;
    public static final int TUNAIKU = 25;
    public static final int PESAWAT = 26;
    public static final int KERETA = 27;
    public static final int PELNI = 28;
    public static final int ADDMENU = 29;
    public static final int PERTAGAS = 30;
    public static final int GAS = 31;
    public static final int PAJAK = 32;
    public static final int PKB = 33;
}
