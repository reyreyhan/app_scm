package com.bm.main.fpl.constants;

/**
 * Created by sarifhidayat on 10/24/18.
 **/
public class EventParam {

    public static final String EVENT_SUCCESS = "YES";
    public static final String EVENT_NOT_SUCCESS = "NO";
    public static final String EVENT_ACTION_VISIT = "VISIT";
    public static final String EVENT_ACTION_REQUEST_LOGIN = "REQUEST_LOGIN";
    public static final String EVENT_ACTION_REQUEST_KEY = "REQUEST_KEY";
    public static final String EVENT_ACTION_CHOICE = "CHOICE";
    public static final String EVENT_ACTION_REQUEST_INQ = "REQUEST INQ";
    public static final String EVENT_ACTION_REQUEST_PAY = "REQUEST PAY";
    public static final String EVENT_ACTION_RESULT_INQ = "RESULT INQ";
    public static final String EVENT_ACTION_RESULT_PAY = "RESULT PAY";
    public static final String EVENT_ACTION_RESULT_LOGIN = "RESULT LOGIN";
    public static final String EVENT_ACTION_RESULT_KEY = "RESULT KEY";

    public static final String EVENT_ACTION_REQUEST_REGISTRASI ="REQUEST REGISTRASI" ;
    public static final String EVENT_ACTION_RESULT_REGISTRASI ="RESULT REGISTRASI" ;
    public static final String EVENT_ACTION_REQUEST_AJAKBISNIS = "REQUEST AJAK BISNIS";
    public static final String EVENT_ACTION_RESULT_AJAKBISNIS = "RESULT AJAK BISNIS";
    public static final String EVENT_ACTION_REQUEST_DEPOSIT = "REQUEST DEPOSIT";
    public static final String EVENT_ACTION_RESULT_DEPOSIT = "RESULT DEPOSIT";
    public static final String EVENT_ACTION_RECEIVED ="RECEIVED POPUP PROMO " ;
    public static final String EVENT_ACTION_SEND ="SEND POPUP PROMO " ;
    public static final String EVENT_ACTION_DENIED ="DENIED POPUP PROMO " ;

    public static final String EVENT_ACTION_REQUEST_SEARCH = "REQUEST SEARCH";
    public static final String EVENT_ACTION_RESULT_SEARCH = "RESULT SEARCH";
    public static final String EVENT_ACTION_REQUEST_FARE = "REQUEST FARE";
    public static final String EVENT_ACTION_RESULT_FARE = "RESULT FARE";
    public static final String EVENT_ACTION_REQUEST_CHANGE_SEAT = "REQUEST CHANGE SEAT";
    public static final String EVENT_ACTION_RESULT_CHANGE_SEAT = "RESULT CHANGE SEAT";
    public static final String EVENT_ACTION_REQUEST_BOOK = "REQUEST CHANGE BOOK";
    public static final String EVENT_ACTION_RESULT_BOOK = "RESULT CHANGE BOOK";
    public static final String EVENT_ACTION_REQUEST_PAYMENT = "REQUEST PAYMENT";
    public static final String EVENT_ACTION_REQUEST_PAYMENT_FROM_PESANANSAYA = "REQUEST PAYMENT FROM PESANAN SAYA";
    public static final String EVENT_ACTION_RESULT_PAYMENT = "RESULT PAYMENT";
    public static final String EVENT_ACTION_RESULT_PAYMENT_FROM_PESANANSAYA = "RESULT PAYMENT FROM PESANAN SAYA";

    public static final String EVENT_ACTION_ADD_PRODUCT = "REQUEST ADD PRODUCT";
    public static final String EVENT_ACTION_SELL_PRODUCT = "REQUEST SELL PRODUCT";
    public static final String EVENT_ACTION_CHOICE_PRODUCT = "REQUEST CHOICE PRODUCT";
    public static final String EVENT_ACTION_REQUEST_KONFIRMASI = "REQUEST KONFIRMASI DEPOSIT";
    public static final String EVENT_ACTION_RESULT_KONFIRMASI = "RESULT KONFIRMASI DEPOSIT";
}
