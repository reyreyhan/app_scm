package com.bm.main.single.ftl.flight.constants;

/**
 * Created by sarifhidayat on 2/6/19.
 **/
public class FlightKeyPreference {

    public static final String dataConfigFlight="dataConfigFlight";
    public static final String dataSettingsFlight="dataSettingsFlight";
    public static final  String airPortListData ="airPortListData";
    public static final String airportKotaAsal="airportKotaAsal";

    public static final String airportNamaAsal="airportNamaAsal";
    public static final String airportKodeAsal="airportKodeAsal";
    public static final String airportGroupAsal="airportGroupAsal";
    public static final String airportKotaTujuan="airportKotaTujuan";
    public static final String airportKodeTujuan="airportKodeTujuan";
    public static final String airportGroupTujuan="airportGroupTujuan";
    public static final String airportNamaTujuan="airportNamaTujuan";
    public static final String departureDateFlight="departureDateFlight";
    public static final String departureDateShowFlight="departureDateShowFlight";
    public static final String returnDateFlight="returnDateFlight";
    public static final String returnDateShowFlight="returnDateShowFlight";
    public static final String countAdultFlight="countAdultFlight";
    public static final String countChildFlight="countChildFlight";
    public static final String countInfantFlight="countInfantFlight";
    public static final String isFilteredAirline="isFilteredAirline";
    public static final String isFilteredPrice="isFilteredPrice";
    public static final String hasilFilterAirlines = "hasilFilterAirlines";
    public static final String airlineCode = "airlineCode";
    public static final String seat_flights = "seat_flights";
    public static final String airlineName = "airlineName";
    public static final String flightCode = "flightCode";
    public static final String departureDate = "departureDate";
    public static final String arrivalDate = "arrivalDate";
    public static final String departureTime = "departureTime";
    public static final String arrivalTime = "arrivalTime";
    public static final String OneWayPriceSchedule = "OneWayPriceSchedule";
    public static final String isTransit = "isTransit";
    public static final String detailTitle = "detailTitle";
    public static final String baggage = "baggage";
    public static final String baggage1 = "baggage1";
    public static final String baggage2 = "baggage2";
    public static final String baggage3 = "baggage3";

    public static final String baggageRoute1 = "baggageRoute1";
    public static final String baggageRoute2 = "baggageRoute2";
    public static final String baggageRoute3 = "baggageRoute3";

    public static final String baggageFlightNo1 = "baggageFlightNo1";
    public static final String baggageFlightNo2 = "baggageFlightNo2";
    public static final String baggageFlightNo3 = "baggageFlightNo3";

 public static final String baggageFlightIcon1 = "baggageFlightIcon1";
    public static final String baggageFlightIcon2 = "baggageFlightIcon2";
    public static final String baggageFlightIcon3 = "baggageFlightIcon3";

    public static final String countBaggage = "countBaggage";


    public static final String baggageAdult1 = "baggageAdult1";
    public static final String baggageAdult2 = "baggageAdult2";
    public static final String baggageAdult3 = "baggageAdult3";
    public static final String baggageAdult4 = "baggageAdult4";
    public static final String baggageAdult5 = "baggageAdult5";
    public static final String baggageAdult6 = "baggageAdult6";
    public static final String baggageAdult7 = "baggageAdult7";


    public static final String baggageChild1 = "baggageChild1";
    public static final String baggageChild2 = "baggageChild2";
    public static final String baggageChild3 = "baggageChild3";
    public static final String baggageChild4 = "baggageChild4";
    public static final String baggageChild5 = "baggageChild5";
    public static final String baggageChild6 = "baggageChild6";
    public static final String baggageChild7 = "baggageChild7";


    public static final String pricefrombook = "pricefrombook";
    public static final String bookcode = "bookcode";
    public static final String isNewFlight ="isNewFlight";
    public static final String resultFilterAirlinesCode ="resultFilterAirlinesCode";
    public static final String resultFilterAirlinesName ="resultFilterAirlinesName";


//    public static final String schedule_list_one_way="schedule_list_one_way";
    public static final String arrlist_schedule_list_one_way="arrlist_schedule_list_one_way";
    public static final String data_on_click="data_on_click";
    public static final String arrlist_airlines_list="arrlist_airlines_list";
    public static final String arrlist_listAirlinesPrice="arrlist_listAirlinesPrice";
    public static final String priceFromFare="priceFromFare";
    public static final String searchChoicePrice="searchChoicePrice";
    public static final String isInternational="isInternational";
    public static final String dataPenumpangDewasa="dataPenumpangDewasa";
    public static final String dataPenumpangAnak="dataPenumpangAnak";
    public static final String dataPenumpangBayi="dataPenumpangBayi";


    public static final String namaPenumpangAdult1="namaPenumpangAdult1";
    public static final String namaPenumpangAdult2="namaPenumpangAdult2";
    public static final String namaPenumpangAdult3="namaPenumpangAdult3";
    public static final String namaPenumpangAdult4="namaPenumpangAdult4";
    public static final String namaPenumpangAdult5="namaPenumpangAdult5";
    public static final String namaPenumpangAdult6="namaPenumpangAdult6";
    public static final String namaPenumpangAdult7="namaPenumpangAdult7";

    public static final String namaPenumpangInfant1="namaPenumpangInfant1";
    public static final String namaPenumpangInfant2="namaPenumpangInfant2";
    public static final String namaPenumpangInfant3="namaPenumpangInfant3";
    public static final String namaPenumpangInfant4="namaPenumpangInfant4";
    public static final String namaPenumpangInfant5="namaPenumpangInfant5";
    public static final String namaPenumpangInfant6="namaPenumpangInfant6";
    public static final String namaPenumpangInfant7="namaPenumpangInfant7";


}
