package com.bm.main.single.ftl.constants;

/**
 * Created by sarifhidayat on 1/24/19.
 **/
public class TravelPath {

    public static final String LOGIN = "app/sign_in";
    public static final String DATA_PAYMENT = "app/data_payment";
    public static final String LIST_BAGGAGE = "app/list_baggage";

    public static final String TRANSACTIONINFO = "app/transaction_info";
    public static final String TRANSACTIONBOOKLIST = "app/transaction_book_list";
    public static final String TRANSACTIONLIST = "app/transaction_list";
    public static final String GLOBAL_PAYMENT = "app/global_payment";
    public static final String FLIGHT_FEE = "flight/fee";
}
