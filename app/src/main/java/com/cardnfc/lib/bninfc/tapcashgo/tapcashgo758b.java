package com.cardnfc.lib.bninfc.tapcashgo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface tapcashgo758b {
    boolean tapcashgo758b_classa();
}
